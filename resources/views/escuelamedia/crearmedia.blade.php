@extends('layouts.app')

@section('content')
<div class="jumbotron jumbotron-fluid" style="background:#8398F5; margin-right: 65px; margin-left: 65px">
  <div class="container fluid">
    <h4 class="display-4">Solicitud Escuela Superior</h1>
    <p class="lead">Es necesario llenar todos los campos solicitados y subir los archivos para ser verificado.</p>
  </div>
</div>


<div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-body" >
            <form>
                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de Solicitud de reconocimiento de validez oficial de estudios (Formato 1):</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">En caso de instituciones nuevas, deberán proponer una terna en orden de preferencia, anexando biografías, monografías y la justificación, así como la bibliografía que sirva como fuente de consulta:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de Solicitud de reconocimiento de validez oficial de estudios (Formato 1):Para el caso de personas morales, deberán presentar copia del acta constitutiva, de lo contrario identificación oficial de quien pretende incorporar:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de Carta de obligatoriedad de los compromisos adquiridos por el plantel y sus diferentes niveles jerárquicos de autoridad, firmada por el representante legal:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Justificación (conforme lo previsto en los artículos 14 y 15 del acuerdo 17/11/17):</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de Plan de estudios. (Anexo1):</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de programa de estudios (Anexo 3):</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de Mapa curricular:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de Listado de acervo bibliográfico:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Reglamento interno del plantel (atendiendo lo previsto en el artículo 46 del acuerdo 17/11/17):</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de plantilla personal directivo / docente:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Listado del inventario del equipo, mobiliario y material didáctico del plantel:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Anexo 4. plataforma tecnológica educativa para modalidades mixtas y no escolarizadas (en caso de ser plataforma propia o en renta, presentar documentación que lo acredite):</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Formato de descripción de instalaciones y descripción de instalaciones especiales (anexo 5):</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Presentar debidamente llenado el formato de legal ocupación del predio:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Presentar el documento que acredite la posesión legal del inmueble:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Presentar el permiso de uso de suelo, firmados por la autoridad correspondiente:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Presentar la constancia de seguridad estructural:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Presentar en digital la anuencia de protección civil:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Presentar el certificado expedido por el instituto de infraestructura física educativa del estado de quintana roo que acredite la calidad de infraestructura del inmueble destinado a fungir como institución educativa:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>

                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                   <label for="exampleFormControlInput1">Presentar debidamente llenado el formato de ratificación:</label>
                  </div>
                  <br>
                  <div class="custom-file">
                    <input type="file" class="form-control-file" id="exampleFormControlFile1">
                  </div>
                </div>
                <br>


                <div class="form-group">
                  <div class="card-header" style="background: #071349; color:white;">
                    <label for="exampleFormControlSelect1">Tipo de persona:</label>
                  </div>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>Moral</option>
                    <option>Fisica</option>
                  </select>
                </div>
                <br>
                <br>
                <button type="submit" class="btn btn-primary">Enviar</button>
                <button type="reset" class="btn btn-danger">Limpiar Filtros</button>
              </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
